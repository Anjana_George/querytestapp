//
//  UserData.swift
//  QueryApp
//
//  Created by Anjana on 3/4/20.
//  Copyright © 2020 Anjana. All rights reserved.
//

import Foundation

struct UserData: Codable {
    
    let gender: String?
    let name : Name?
    let email : String?
    let dob: DOB?
    let login : Login?
    
    enum CodingKeys: String, CodingKey {
        case gender = "gender"
        case name = "name"
        case email = "email"
        case dob = "dob"
        case login = "login"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        name = try values.decodeIfPresent(Name.self, forKey: .name)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        dob = try values.decodeIfPresent(DOB.self, forKey: .dob)
        login = try values.decodeIfPresent(Login.self, forKey: .login)
    }
    
}

struct Name: Codable {
    
    let title: String?
    let first: String?
    let last: String?
    
    enum CodingKeys: String, CodingKey {
        case title = "title"
        case first = "first"
        case last = "last"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        first = try values.decodeIfPresent(String.self, forKey: .first)
        last = try values.decodeIfPresent(String.self, forKey: .last)
    }
    
}

struct DOB: Codable {
    
    let date: String?
    let age: Int?
    
    enum CodingKeys: String, CodingKey {
        case date = "date"
        case age = "postcode"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        age = try values.decodeIfPresent(Int.self, forKey: .age)
    }
}

struct Login: Codable {
    
    let uuid: String?
    let username: String?
    
    enum CodingKeys: String, CodingKey {
        case uuid = "uuid"
        case username = "username"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        uuid = try values.decodeIfPresent(String.self, forKey: .uuid)
        username = try values.decodeIfPresent(String.self, forKey: .username)
    }
}
