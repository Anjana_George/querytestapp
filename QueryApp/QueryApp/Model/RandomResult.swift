//
//  RandomResult.swift
//  QueryApp
//
//  Created by Telepod on 3/4/20.
//  Copyright © 2020 Anjana. All rights reserved.
//

import Foundation

struct RandomResult: Codable {
    
    let results: [UserData]?
    let info : Info?
    
    enum CodingKeys: String, CodingKey {
        case results = "results"
        case info = "info"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        results = try values.decodeIfPresent([UserData].self, forKey: .results)
        info = try values.decodeIfPresent(Info.self, forKey: .info)
    }
    
}
