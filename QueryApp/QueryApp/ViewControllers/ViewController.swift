//
//  ViewController.swift
//  QueryApp
//
//  Created by Telepod on 3/4/20.
//  Copyright © 2020 Anjana. All rights reserved.
//

import UIKit
import LocalAuthentication
import APIManager

class ViewController: UIViewController {
    
    let rest = APIManager()

    var gender = "male"
    
    @IBOutlet weak var seedTextField: UITextField!
    @IBOutlet weak var multiUserTextField: UITextField!
    @IBOutlet weak var genderControl: UISegmentedControl!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var queryButton: UIButton!
    
    var randomResult: RandomResult?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
        }else{
            alert(title: "No Network Connection!", message: "Please check you network connection and try again!")
        }
        
        //self.queryButton.isHidden = true
        //self.authenticationWithTouchID()
        //To enable authentication block uncomment above 2 lines of code and delete menu button code below
        
        //set menu button click
        if self.revealViewController() != nil {
            self.menuButton.target = self.revealViewController()
            self.menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        
    }
    
    func authenticationWithTouchID() {
        let localAuthenticationContext = LAContext()
        localAuthenticationContext.localizedFallbackTitle = "Please use your Passcode"
        var authorizationError: NSError?
        let reason = "Authentication required to access the secure data"
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthentication, error: &authorizationError) {
            localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason) { success, evaluateError in
                if success {
                    DispatchQueue.main.async() {self.alert(title: "Success", message: "Authenticated succesfully!")}
                    self.queryButton.isHidden = false
                    //set menu button click
                    if self.revealViewController() != nil {
                        self.menuButton.target = self.revealViewController()
                        self.menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
                        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                    }
                } else {
                    // Failed to authenticate
                    guard let error = evaluateError else {
                        DispatchQueue.main.async() {self.alert(title: "Error!", message: "Authentication unsuccesful! You cannot use this any of the features in this app.")}
                        return
                    }
                    print(error)
                    
                }
            }
        } else {
            guard let error = authorizationError else {
                DispatchQueue.main.async() {self.alert(title: "Error!", message: "Authentication unsuccesful! You cannot use this any of the features in this app.")}
                return
            }
            print(error)
        }
    }
    
    @IBAction func actQuery(_ sender: UIButton) {
        switch genderControl.selectedSegmentIndex {
        case 0: gender = "male";
        case 1: gender = "female";
        default: break;
        }
        if let num = multiUserTextField.text, let numUsers = Int(num), numUsers > 0 {
            print(numUsers)
            //Call multiple random users
            self.getUsers(num: num)
        }else if let seed = seedTextField.text, !seed.isEmpty {
            //Call random user by Seed
            self.getUserBySeed(seed: seed)
        }else {
            //Call random user by gender
            self.getUserByGender()
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "userDetailsSegue", let destinationVC = segue.destination as? ViewController_UserDetails {
            destinationVC.randomResult = self.randomResult
        }else if segue.identifier == "segueUsersTable", let destinationVC = segue.destination as? ViewController_QueryUsersTable {
            destinationVC.randomResult = self.randomResult
        }
    }
    
    func getUserByGender() {
        guard let url = URL(string: "https://randomuser.me/api") else { return }
        rest.urlQueryParameters.add(value: self.gender, forKey: "gender")
        rest.makeRequest(toURL: url, withHttpMethod: .get) { (results) in
            if let response = results.response {
                if response.httpStatusCode != 200 {
                    DispatchQueue.main.async {self.alert(title: "Error \(response.httpStatusCode)", message: "Request failed with HTTP status code \(response.httpStatusCode)")}
                }else {
                    if let data = results.data {
                        let decoder = JSONDecoder()
                        guard let dataReturned = try? decoder.decode(RandomResult.self, from: data) else { DispatchQueue.main.async {self.alert(title: "API : Error Returned", message: "Something went wrong. Please try again!")}
                            return
                        }
                        self.randomResult = dataReturned
                        print(dataReturned)
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "userDetailsSegue", sender: self)
                        }
                    }
                }
            }
        }
    }
    
    func getUserBySeed(seed: String) {
        guard let url = URL(string: "https://randomuser.me/api/") else { return }
        rest.urlQueryParameters.add(value: seed, forKey: "seed")
        rest.makeRequest(toURL: url, withHttpMethod: .get) { (results) in
            if let response = results.response {
                if response.httpStatusCode != 200 {
                    DispatchQueue.main.async {self.alert(title: "Error \(response.httpStatusCode)", message: "Request failed with HTTP status code \(response.httpStatusCode)")}
                }else {
                    if let data = results.data {
                        let decoder = JSONDecoder()
                        guard let dataReturned = try? decoder.decode(RandomResult.self, from: data) else { DispatchQueue.main.async {self.alert(title: "API : Error Returned", message: "Try again with another seed!")}
                            return
                        }
                        self.randomResult = dataReturned
                        print(dataReturned)
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "userDetailsSegue", sender: self)
                        }
                    }
                    
                }
            }
        }
    }
    
    func getUsers(num : String) {
        guard let url = URL(string: "https://randomuser.me/api/") else { return }
        rest.urlQueryParameters.add(value: num, forKey: "results")
        rest.makeRequest(toURL: url, withHttpMethod: .get) { (results) in
            if let response = results.response {
                if response.httpStatusCode != 200 {
                    DispatchQueue.main.async {self.alert(title: "Error \(response.httpStatusCode)", message: "Request failed with HTTP status code \(response.httpStatusCode)")}
                }else {
                    if let data = results.data {
                        let decoder = JSONDecoder()
                        guard let dataReturned = try? decoder.decode(RandomResult.self, from: data) else { DispatchQueue.main.async {self.alert(title: "API : Error Returned", message: "Try again later!")}
                            return
                        }
                        self.randomResult = dataReturned
                        print(dataReturned)
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "segueUsersTable", sender: self)
                        }
                    }
                    
                }
            }
        }
    }
    
    func alert(title : String, message : String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

