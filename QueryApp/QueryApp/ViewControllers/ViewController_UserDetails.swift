//
//  ViewController_UserDetails.swift
//  QueryApp
//
//  Created by Telepod on 4/4/20.
//  Copyright © 2020 Anjana. All rights reserved.
//

import UIKit
import CoreData

class ViewController_UserDetails: UIViewController {

    var randomResult: RandomResult?
    var index: Int = 0
    var storedUser: NSManagedObject?

    
    @IBOutlet weak var userIdLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var seedLabel: UILabel!
    
    @IBOutlet weak var actionButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let randomResult = self.randomResult {
            if let users = randomResult.results, let login = users[index].login, let uid = login.uuid {
                userIdLabel.text = uid
            }
            if let users = randomResult.results, let name = users[index].name, let title = name.title, let first = name.first, let last = name.last {
                nameLabel.text = title + " " + first + " " + last
            }
            if let users = randomResult.results, let gender = users[index].gender {
                genderLabel.text = gender
            }
            if let users = randomResult.results, let dob = users[index].dob, let date = dob.date {
                if let birth = self.getDate(dateStr: date) {
                    let ageComponents = Calendar.current.dateComponents([.year], from: birth, to: Date())
                    let age = ageComponents.year!
                    ageLabel.text = String(age)
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd MMMM YYYY"
                    let birthDate = formatter.string(from: birth)
                    dobLabel.text = birthDate
                }
            }
            if let users = randomResult.results, let email = users[index].email {
                emailLabel.text = email
            }
            if let info = randomResult.info, let seed = info.seed {
                 seedLabel.text = seed
            }
        }
        if let user = self.storedUser {
            actionButton.isHidden = true
            userIdLabel.text = user.value(forKeyPath: "uuid") as? String
            nameLabel.text = user.value(forKeyPath: "name") as? String
            genderLabel.text = user.value(forKeyPath: "gender") as? String
            ageLabel.text = user.value(forKeyPath: "age") as? String
            dobLabel.text = user.value(forKeyPath: "dob") as? String
            emailLabel.text = user.value(forKeyPath: "email") as? String
            seedLabel.text = user.value(forKeyPath: "seed") as? String
            
        }
    }
    
    func getDate(dateStr : String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//        dateFormatter.timeZone = TimeZone.current
//        dateFormatter.locale = Locale.current
        let orgDate = dateFormatter.date(from: dateStr)
        print(dateStr)
        return orgDate
    }
    
    
    @IBAction func actStore(_ sender: UIButton) {
        if let name = nameLabel.text, let gender = genderLabel.text, let ageStr = ageLabel.text, let age = Int(ageStr), let dob = dobLabel.text, let email = emailLabel.text, let uuid = userIdLabel.text, let seed = seedLabel.text {
            self.save(name: name, gender : gender, age: age, dob : dob, email : email, uuid : uuid, seed: seed)
        }
    }

    func save(name: String, gender : String, age: Int, dob : String, email : String, uuid : String, seed : String) {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        // 1
        let managedContext = appDelegate.persistentContainer.viewContext
        
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "User",
                                               in: managedContext)!
        let person = NSManagedObject(entity: entity,insertInto: managedContext)
        
        // 3
        person.setValue(name, forKeyPath: "name")
        person.setValue(gender, forKeyPath: "gender")
        person.setValue(age, forKeyPath: "age")
        person.setValue(dob, forKeyPath: "dob")
        person.setValue(email, forKeyPath: "email")
        person.setValue(uuid, forKeyPath: "uuid")
        person.setValue(seed, forKeyPath: "seed")
        
        // 4
        do {
            try managedContext.save()
            self.alert(title: "Successfully Saved Data", message: "We have successfully saved user data locally.")
        } catch let error as NSError {
            print("\(error), \(error.userInfo)")
            self.alert(title: "Failed to Saved Data", message: "Could not save. The user data already exist in database.")
        }
    }
    
    func alert(title : String, message : String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

