//
//  ViewController_StoredUsersTable.swift
//  QueryApp
//
//  Created by Telepod on 4/4/20.
//  Copyright © 2020 Anjana. All rights reserved.
//

import UIKit
import CoreData

class ViewController_StoredUsersTable: UIViewController,  UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    var arrayOfUsers:[NSManagedObject] = []
    var selectedUser:NSManagedObject?
    override func viewDidLoad() {
        super.viewDidLoad()
        //set menu button click
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        //get data
        self.getUsers()
        
    }
    
    func getUsers() {
        //1
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "User")
        
        //3
        do {
            arrayOfUsers = try managedContext.fetch(fetchRequest)
            self.tableView.reloadData()
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "usercell", for: indexPath) as! TableViewCell_User
        let user = arrayOfUsers[indexPath.row]
        
        cell.name.text = user.value(forKeyPath: "name") as? String
        cell.gender.text = user.value(forKeyPath: "gender") as? String
        cell.email.text = user.value(forKeyPath: "email") as? String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        selectedUser = self.arrayOfUsers[row]
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "storedUserSegue", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         if segue.identifier == "storedUserSegue", let destinationVC = segue.destination as? ViewController_UserDetails {
            destinationVC.storedUser = self.selectedUser
         }
     }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // remove the deleted item from the model
            guard let appDelegate =  UIApplication.shared.delegate as? AppDelegate else {return}
            let context = appDelegate.persistentContainer.viewContext
            context.delete(arrayOfUsers[indexPath.row] )
            do {
                try context.save()
            } catch _ {
                self.alert(title: "Failed to Delete Data", message: "Unknown error. Please try again later!")
            }
            
            self.arrayOfUsers.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            
        }
    }
    
    func alert(title : String, message : String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Click", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
