//
//  ViewController_QueryUsersTable.swift
//  QueryApp
//
//  Created by Telepod on 5/4/20.
//  Copyright © 2020 Anjana. All rights reserved.
//

import UIKit

class ViewController_QueryUsersTable: UIViewController,  UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var tableView: UITableView!
    
    var randomResult: RandomResult?

    var arrayOfUsers:[UserData] = []
    
    var selectedIndex: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        if let randomResult = self.randomResult, let users = randomResult.results {
            arrayOfUsers = users
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "usercell", for: indexPath) as! TableViewCell_User
        let user = arrayOfUsers[indexPath.row]
    
        if let name = user.name, let title = name.title, let first = name.first, let last = name.last {
            cell.name.text = title + " " + first + " " + last
        }
        if let gender = user.gender {
            cell.gender.text = gender
        }
        if let email = user.email {
            cell.email.text = email
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "segueShowUser", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         if segue.identifier == "segueShowUser", let destinationVC = segue.destination as? ViewController_UserDetails {
            destinationVC.randomResult = self.randomResult
            destinationVC.index = self.selectedIndex
         }
     }
}
