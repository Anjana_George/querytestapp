//
//  APIManager.swift
//  QueryApp
//
//  Created by Telepod on 3/4/20.
//  Copyright © 2020 Anjana. All rights reserved.
//

import Foundation
public class APIManager {
    var httpBody: Data?
    
    public var requestHttpHeaders = APIEntity()
    
    public var urlQueryParameters = APIEntity()
    
    public var httpBodyParameters = APIEntity()
    
    public init() {}
    
    //    They are all of the same custom type, but each one is meant to be used for a different concept of a web request.
    //     Setting a request HTTP header.
    //    requestHttpHeaders.add(value: "application/json", forKey: "content-type")
    //     Getting a URL query parameter.
    //    urlQueryParameters.value(forKey: "firstname")
    
    private func addURLQueryParameters(toURL url: URL) -> URL {
        //accepts a URL value, and returns URL
        if urlQueryParameters.totalItems() > 0 {
            //Check URL query parameters to append to the query
            guard var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else { return url }
            var queryItems = [URLQueryItem]()
            for (key, value) in urlQueryParameters.allValues() {
                let item = URLQueryItem(name: key, value: value.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))
                queryItems.append(item)
            }
            urlComponents.queryItems = queryItems
            guard let updatedURL = urlComponents.url else { return url }
            return updatedURL
        }
        return url
    }
    
    private func getHttpBody() -> Data? {
        guard let contentType = requestHttpHeaders.value(forKey: "Content-Type") else { return nil }
        
        if contentType.contains("application/json") {
            //Body eith Json Object
            return try? JSONSerialization.data(withJSONObject: httpBodyParameters.allValues(), options: [.prettyPrinted, .sortedKeys])
        } else if contentType.contains("application/x-www-form-urlencoded") {
            //Body url encoded
            let bodyString = httpBodyParameters.allValues().map { "\($0)=\(String(describing: $1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)))" }.joined(separator: "&")
            return bodyString.data(using: .utf8)
        } else {
            return httpBody
        }
    }
    
    private func prepareRequest(withURL url: URL?, httpBody: Data?, httpMethod: HttpMethod) -> URLRequest? {
        guard let url = url else { return nil }
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
     
        for (header, value) in requestHttpHeaders.allValues() {
            request.setValue(value, forHTTPHeaderField: header)
        }
     
        request.httpBody = httpBody
        return request
    }
    
    //Making Web Requests
    public func makeRequest(toURL url: URL,
                     withHttpMethod httpMethod: HttpMethod,
                     completion: @escaping (_ result: Results) -> Void) {
        //we’ll perform all actions asynchronously in a background thread, so the main thread remains free to be used by the app
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            let targetURL = self?.addURLQueryParameters(toURL: url)
            let httpBody = self?.getHttpBody()
            guard let request = self?.prepareRequest(withURL: targetURL, httpBody: httpBody, httpMethod: httpMethod) else {
                completion(Results(withError: CustomError.failedToCreateRequest))
                return
            }
            
            let session = URLSession.shared
            let task = session.dataTask(with: request) { (data, response, error) in
                completion(Results(withData: data,
                                   response: Response(fromURLResponse: response),
                                   error: error))
            }
            task.resume()
        }
    }
    
    //Fetching Data From URLs
    public func getData(fromURL url: URL, completion: @escaping (_ data: Data?) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            let session = URLSession.shared
            let task = session.dataTask(with: url, completionHandler: { (data, response, error) in
                guard let data = data else { completion(nil); return }
                completion(data)
            })
            task.resume()
        }
    }
}

extension APIManager {
    public enum HttpMethod: String {
        case get
        case post
        case put
        case patch
        case delete
    }
    
    public struct APIEntity {
        private var values: [String: String] = [:]
     
        public mutating func add(value: String, forKey key: String) {
            values[key] = value
        }
     
        func value(forKey key: String) -> String? {
            return values[key]
        }
     
        func allValues() -> [String: String] {
            return values
        }
     
        func totalItems() -> Int {
            return values.count
        }
    }
    
    public struct Response {
        var response: URLResponse? //We will keep the actual response object (URLResponse) in it. Note that this object does not contain the actual data returned from server.
        public var httpStatusCode: Int = 0 //The status code (2xx, 3xx, etc) that represents the outcome of the request.
        var headers = APIEntity() //An instance of the APIEntity struct.
        
        init(fromURLResponse response: URLResponse?) {
            guard let response = response else { return }
            self.response = response
            httpStatusCode = (response as? HTTPURLResponse)?.statusCode ?? 0
            if let headerFields = (response as? HTTPURLResponse)?.allHeaderFields {
                for (key, value) in headerFields {
                    headers.add(value: "\(value)", forKey: "\(key)")
                }
            }
        }
    }
    
    public struct Results {
        public var data: Data?
        public var response: Response?
        public var error: Error?
        
        init(withData data: Data?, response: Response?, error: Error?) {
               self.data = data
               self.response = response
               self.error = error
           }
        
           init(withError error: Error) {
               self.error = error
           }
    }
    
    
    enum CustomError: Error {
        case failedToCreateRequest
    }
}

extension APIManager.CustomError: LocalizedError {
    //CustomError enum in order to provide a localized description of error
    public var localizedDescription: String {
        switch self {
        case .failedToCreateRequest: return NSLocalizedString("Unable to create the URLRequest object", comment: "")
        }
    }
}
